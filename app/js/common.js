$(function () {
	var time = 4;
	var $bar,
		$slick,
		isPause,
		tick,
		percentTime;

	$slick = $('.slider');
	$slick.slick({
		// draggable: true,
		// adaptiveHeight: true,
		// infinite: true,
		// dots: true,
		// mobileFirst: true,
		// pauseOnDotsHover: true,
		// cssEase: 'linear',
		// autoplay: true,
		// autoplaySpeed: 4000,
		// dots: true
		slidesToShow: 1,
		dots: true,
		// centerMode: true,
	});

	$('.carousel').slick({
		slidesToShow: 3,
		dots: true,
		centerMode: true,
	});
	// speed: 300,


	$bar = $('.slider-progress .progress');
	$barRound = $('.progress');

	$('.slider-wrapper').on({
		mouseenter: function () {
			isPause = true;
		},
		mouseleave: function () {
			isPause = false;
		}
	})

	function startProgressbar() {
		resetProgressbar();
		$('')
		percentTime = 0;
		isPause = false;
		tick = setInterval(interval, 10);
	}
	var $rbar = $('.progress circle');
	var rlen = 2 * Math.PI * $rbar.attr('r');
	function interval() {

		percentTime += 1 / (time + 0.1);
		$bar.css({
			width: percentTime + '%'
		});
		$rbar.css({
			'stroke-dasharray': rlen,
			'stroke-dashoffset': rlen * (1 - percentTime / 100)
		});

		if (percentTime >= 100) {
			$slick.slick('slickNext');
			startProgressbar();
		}

	}
	function resetProgressbar() {
		$bar.css({
			width: 0 + '%'
		});
		clearTimeout(tick);
	}

	startProgressbar();



	var sliderTime = $("#slider-time").slider({
		range: "min",
		value: 0,
		min: 0,
		max: 100,
		step: 1,

		slide: function (event, ui) {
			$(ui.handle).find('.tooltip').text(ui.value);
		},
		create: function (event, ui) {
			var tooltip = $('<div class="tooltip" />');

			$(event.target).find('.ui-slider-handle').append(tooltip);
		},
		change: function (event, ui) {
			$('#hidden').attr('value', ui.value);
		}
	});
	var sliderMoney = $("#slider-money").slider({
		range: "min",
		value: 0,
		min: 0,
		max: 100,
		step: 1,

		slide: function (event, ui) {
			$(ui.handle).find('.tooltip').text(ui.value);
		},
		create: function (event, ui) {
			var tooltip = $('<div class="tooltip" />');

			$(event.target).find('.ui-slider-handle').append(tooltip);
		},
		change: function (event, ui) {
			$('#hidden').attr('value', ui.value);
		}
	});



	var mySwiper = new Swiper('.swiper-container', {
		// Optional parameters
		// direction: 'vertical',
		// loop: true,

		// If we need pagination
		// pagination: {
		// 	el: '.swiper-pagination',
		// },

		// Navigation arrows
		breakpoints: {
			320: {
				slidesPerView: 1,
				spaceBetween: 0,
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 0,
			},
			976: {
				slidesPerView: 4,
				spaceBetween: 0,
			},
			1445: {
				slidesPerView: 4,
				spaceBetween: 0,
			},
			1920: {
				slidesPerView: 4,
				spaceBetween: 0,
			},
		},
		// navigation: {
		// 	nextEl: '.swiper-button-next',
		// 	prevEl: '.swiper-button-prev',
		// },
	})

	$(".slick-card").slick({
		// normal options...
		infinite: false,
		adaptiveHeight: true,
		centerMode: true,
		arrows: false,
		// the magic
		responsive: [{
			breakpoint: 1024,
			settings: {
				slidesToShow: 4,
				infinite: false
			}
		},
		{
			breakpoint: 600,
			settings: {
				slidesToShow: 2,
				infinite: true
				// dots: true
			}
		},
		{
			breakpoint: 300,
			// settings: "unslick" // destroys slick
			settings: {
				slidesToShow: 1,
				infinite: true
			}
		}]
	});
});
